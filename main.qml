import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import "controls" as Awesome

ApplicationWindow {
    title: qsTr("Twin Strangers")
    width: 640
    height: 480
    visible: true

    FontAwesome {
        id: fa
        resource: "qrc:/fontawesome-webfont.ttf"
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Help")
            MenuItem {
                text: qsTr("&About")
                onTriggered: messageDialog.show(qsTr("About Page"));
            }

        }
    }
    toolBar: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                //iconSource: fa.icons.fa_arrow_left
                text: "  Login"
                onClicked: {
                    if (!stackview.busy) {
                        stackview.forceActiveFocus();
                        stackview.pop();
                    }
                }
                visible: stackview.depth > 1
            }
            Item {
                id: space
                Layout.fillWidth: true
            }
            ToolButton {
                text: "SIGN UP"
                visible: stackview.depth === 1
                onClicked: {
                    if (!stackview.busy)
                        stackview.push(Qt.resolvedUrl("RegisterScreen.qml"));
                }
            }

        }
    }

    /*SwipeView {
        anchors.fill: parent
        model: ListModel {
            ListElement {
                title: qsTr("Log In")
                source: "qrc:/LogInScreen.qml"
            }
            ListElement {
                title: qsTr("Register")
                source: "qrc:/RegisterScreen.qml"
            }
        }
    }*/

    StackView {
        id: stackview
        anchors.fill: parent
        initialItem: LogInScreen {}
        focus: true
        Keys.onReleased: {
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
                if(stackview.depth > 1) {
                    stackview.pop();
                    event.accepted = true;
                } else {
                    Qt.quit();
                }
            }
        }

    }

    /*MainForm {
        anchors.fill: parent
        button1.onClicked: messageDialog.show(qsTr("Button 1 pressed"))
        button2.onClicked: messageDialog.show(qsTr("Button 2 pressed"))
        button3.onClicked: messageDialog.show(qsTr("Button 3 pressed"))
    }*/

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}
