#-------------------------------------------------
#
# Project created by QtCreator 2015-04-08T16:28:53
#
#-------------------------------------------------

QT       += core gui quick opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qlemon
TEMPLATE = app

LIBS += -lopencv_core

INCLUDEPATH += "$$_PRO_FILE_PWD_/OpenCV-android-sdk/sdk/native/jni/include"

android {
    LIBS += \
        -L"$$_PRO_FILE_PWD_/OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a"\
        -L"$$_PRO_FILE_PWD_/OpenCV-android-sdk/sdk/native/libs/armeabi-v7a"\
        -llibtiff\
        -llibjpeg\
        -llibjasper\
        -llibpng\
        -lIlmImf\
        -ltbb\
        -lopencv_core\
        -lopencv_androidcamera\
        -lopencv_flann\
        -lopencv_imgproc\
        -lopencv_highgui\
        -lopencv_features2d\
        -lopencv_calib3d\
        -lopencv_ml\
        -lopencv_objdetect\
        -lopencv_video\
        -lopencv_contrib\
        -lopencv_photo\
        -lopencv_java\
        -lopencv_legacy\
        -lopencv_ocl\
        -lopencv_stitching\
        -lopencv_superres\
        -lopencv_ts\
        -lopencv_videostab

    ANDROID_PACKAGE_SOURCE_DIR=$$_PRO_FILE_PWD_/android
}


SOURCES += main.cpp

HEADERS  +=

FORMS    +=

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    qml.qrc \
    resource.qrc

